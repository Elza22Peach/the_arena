// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class The_ArenaEditorTarget : TargetRules
{
	public The_ArenaEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		bLegacyPublicIncludePaths = true;
		ShadowVariableWarningLevel = WarningLevel.Warning;

		ExtraModuleNames.AddRange( new string[] { "The_Arena" } );
	}
}
