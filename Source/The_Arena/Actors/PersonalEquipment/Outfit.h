// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "The_Arena/Actors/InventoryItem.h"
#include "Outfit.generated.h"

/**
 *
 */
UCLASS()
class THE_ARENA_API AOutfit : public AInventoryItem
{
	GENERATED_BODY()

	//Methods
public:
	void OutfitInitialization();
	void OutfitDeinitialization();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	USkeletalMesh* GetOutfitSkeletalMesh() const;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetPhysicalProtection() const;
	UFUNCTION(BlueprintCallable)
	void SetPhysicalProtection(float Value);

protected:
	AOutfit();
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION()
	void OnRep_OutfitInitialized();

	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
	USkeletalMesh* Team_1_OutfitSkeletalMesh;
	UPROPERTY(EditDefaultsOnly)
	USkeletalMesh* Team_2_OutfitSkeletalMesh;

	UPROPERTY(ReplicatedUsing = OnRep_OutfitInitialized)
	bool bOutfitInitialized = false;

	FTimerHandle OutfitInitializationHandle;

	float PhysicalProtection = 0.0f;
};
