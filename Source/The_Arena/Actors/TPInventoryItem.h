// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TPInventoryItem.generated.h"

UCLASS()
class THE_ARENA_API ATPInventoryItem : public AActor
{
	GENERATED_BODY()

	//Methods
public:	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	UMeshComponent* GetTPItemMeshComponent() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	TArray<AActor*> GetVisibleActors() const;
protected:
	ATPInventoryItem();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	//Fields
protected:
	UPROPERTY(Replicated, BlueprintReadWrite)
	UMeshComponent* TPItemMeshComponent;
	UPROPERTY(Replicated, BlueprintReadWrite)
	TArray<AActor*> VisibleActors;
};
