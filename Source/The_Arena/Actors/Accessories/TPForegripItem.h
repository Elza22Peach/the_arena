// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "The_Arena/Actors/TPInventoryItem.h"
#include "TPForegripItem.generated.h"

/**
 * 
 */
UCLASS()
class THE_ARENA_API ATPForegripItem : public ATPInventoryItem
{
	GENERATED_BODY()
	
};
