// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Sound/SoundCue.h"

#include "The_Arena/Actors/InventoryItem.h"

#include "SilencerItem.generated.h"

/**
 * 
 */
UCLASS()
class THE_ARENA_API ASilencerItem : public AInventoryItem
{
	GENERATED_BODY()
};
