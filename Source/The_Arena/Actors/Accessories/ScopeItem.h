// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "The_Arena/Actors/InventoryItem.h"
#include "The_Arena/ActorComponents/TransformController.h"
#include "ScopeItem.generated.h"

/**
 * 
 */
UCLASS()
class THE_ARENA_API AScopeItem : public AInventoryItem
{
	GENERATED_BODY()

	//Methods
protected:
	virtual void InitializationOnWeapon() override;
	virtual void DeinitializationFromWeapon() override;
	
	void ScopeInitialization();
	void ScopeDeInitialization();

	UFUNCTION()
	void Aim(bool IsAiming);
	
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
	float AdditionalScopeFOV;
	UPROPERTY(BlueprintReadWrite)
	UTransformController* ScopeTransformController;

	bool bAdditionalScopeFOVAdded = false;

	FTimerHandle ScopeInitializationTimer;
	FTimerHandle ReturnAimingFOVHandle;
};
