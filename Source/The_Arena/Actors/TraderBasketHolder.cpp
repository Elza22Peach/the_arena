// Copyright© Vladimir Dolbnya 2022 All Rights Reserved


#include "The_Arena/Actors/TraderBasketHolder.h"

// Sets default values
ATraderBasketHolder::ATraderBasketHolder()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATraderBasketHolder::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATraderBasketHolder::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

