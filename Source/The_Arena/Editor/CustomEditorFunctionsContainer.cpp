// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomEditorFunctionsContainer.h"

// Sets default values
ACustomEditorFunctionsContainer::ACustomEditorFunctionsContainer()
{

}

// Called when the game starts or when spawned
void ACustomEditorFunctionsContainer::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACustomEditorFunctionsContainer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACustomEditorFunctionsContainer::AddStaticMeshColorComponent_Implementation()
{
	TArray<AActor*> StaticMeshesWithColor;
	UGameplayStatics::GetAllActorsOfClassWithTag(GetWorld(), AActor::StaticClass(), "StaticMeshColor", StaticMeshesWithColor);

	if (StaticMeshesWithColor.Num() > 0)
	{
		for(auto Mesh : StaticMeshesWithColor)
		{
			auto StaticMeshColor = NewObject<UStaticMeshColorComponent>(Mesh, "StaticMeshColor");
			Mesh->AddInstanceComponent(StaticMeshColor);
		}
	}
}

