// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Curves/CurveFloat.h"

#include "The_Arena/Characters/FirstPersonCharacter.h"

#include "WeaponRecoilComponent.generated.h"


USTRUCT(BlueprintType)
struct FRecoilRandomParam
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly)
		bool bUseRandomDistance;

	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bUseRandomDistance"))
		FVector2D RandomDistance;

	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bUseRandomDistance"))
		UCurveFloat* DistanceXCurve;

	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "bUseRandomDistance"))
		UCurveFloat* DistanceYCurve;

	UPROPERTY(EditDefaultsOnly, meta = (EditCondition = "!bUseRandomDistance"))
		UCurveFloat* DistanceCurve;

	UPROPERTY(EditDefaultsOnly)
		UCurveFloat* ForwardSpeedCurve;

	UPROPERTY(EditDefaultsOnly)
		UCurveFloat* BackSpeedCurve;

	UPROPERTY(EditDefaultsOnly)
		float CurrencyToEqualDistances = 0.000001f;
};

USTRUCT(BlueprintType)
struct FRecoilLocationParam
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly)
		FRecoilRandomParam Forward;

	UPROPERTY(EditDefaultsOnly)
		FRecoilRandomParam Right;

	UPROPERTY(EditDefaultsOnly)
		FRecoilRandomParam Up;
};

USTRUCT(BlueprintType)
struct FRecoilRotationParam
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly)
		FRecoilRandomParam Roll;

	UPROPERTY(EditDefaultsOnly)
		FRecoilRandomParam Pitch;

	UPROPERTY(EditDefaultsOnly)
		FRecoilRandomParam Yaw;
};

USTRUCT(BlueprintType)
struct FRecoilCameraUpRotationParam
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly)
		float Rotation;

	UPROPERTY(EditDefaultsOnly)
		UCurveFloat* RotationCurve;

	UPROPERTY(EditDefaultsOnly)
		float ForwardSpeed;

	UPROPERTY(EditDefaultsOnly)
		UCurveFloat* ForwardSpeedCurve;

	UPROPERTY(EditDefaultsOnly)
		float StabilizationSpeed;

	UPROPERTY(EditDefaultsOnly)
		UCurveFloat* StabilizationSpeedCurve;

	UPROPERTY(EditDefaultsOnly)
		float BackSpeed;

	UPROPERTY(EditDefaultsOnly)
		UCurveFloat* BackSpeedCurve;
};

USTRUCT(BlueprintType)
struct FRecoilCameraShakeParam
{
	GENERATED_USTRUCT_BODY()

	FRecoilCameraShakeParam()
	{
		if(RecoilCameraShakeClass)
		{
			if(auto RecoilCameraShake = Cast<UMatineeCameraShake>(RecoilCameraShakeClass.Get()))
			{
				RecoilCameraShake->OscillationDuration = 0.2f;
				RecoilCameraShake->OscillationBlendInTime = 0.1f;
				RecoilCameraShake->OscillationBlendOutTime = 0.1f;

				RecoilCameraShake->RotOscillation.Pitch.InitialOffset = EInitialOscillatorOffset::EOO_OffsetRandom;
				RecoilCameraShake->RotOscillation.Pitch.Waveform = EOscillatorWaveform::SineWave;

				RecoilCameraShake->RotOscillation.Yaw.InitialOffset = EInitialOscillatorOffset::EOO_OffsetRandom;
				RecoilCameraShake->RotOscillation.Yaw.Waveform = EOscillatorWaveform::SineWave;

				RecoilCameraShake->RotOscillation.Roll.InitialOffset = EInitialOscillatorOffset::EOO_OffsetRandom;
				RecoilCameraShake->RotOscillation.Roll.Waveform = EOscillatorWaveform::SineWave;
			}	
		}
	}

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<UMatineeCameraShake> RecoilCameraShakeClass;

	UPROPERTY(EditDefaultsOnly)
		UCurveFloat* RollCurve;

	UPROPERTY(EditDefaultsOnly)
		UCurveFloat* RollSpeedCurve;

	UPROPERTY(EditDefaultsOnly)
		UCurveFloat* PitchCurve;

	UPROPERTY(EditDefaultsOnly)
		UCurveFloat* PitchSpeedCurve;

	UPROPERTY(EditDefaultsOnly)
		UCurveFloat* YawCurve;

	UPROPERTY(EditDefaultsOnly)
		UCurveFloat* YawSpeedCurve;
};


//Rotation coefs from location recoils. For example: LocationRecoil = 30.f; RotationRecoil = LocationRecoil * RotationCoef;
USTRUCT(BlueprintType)
struct FMeshRotationCoefsParam
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	float RollCoef;
	UPROPERTY(EditDefaultsOnly)
	float PitchCoef;
	UPROPERTY(EditDefaultsOnly)
	float YawCoef;
};

UCLASS(ClassGroup = (Custom), Blueprintable, BlueprintType, meta = (BlueprintSpawnableComponent))
class THE_ARENA_API UWeaponRecoilComponent : public UActorComponent
{
	GENERATED_BODY()

		//Methods
public:
	// Sets default values for this component's properties
	UWeaponRecoilComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
		void InitializationParameters(AFirstPersonCharacter* Character, USceneComponent* MeshComponent);

	UFUNCTION(BlueprintCallable)
		void UpdateRecoilParameters(float ScaleOfMoving, bool Aiming);

	UFUNCTION(BlueprintCallable)
		void PlayRecoilShake();

	UFUNCTION(BlueprintCallable)
		float GetVerticalRecoil() const;
	UFUNCTION(BlueprintCallable)
		void SetVerticalRecoil(float VRecoil);

	UFUNCTION(BlueprintCallable)
		float GetHorizontalRecoil() const;
	UFUNCTION(BlueprintCallable)
		void SetHorizontalRecoil(float HRecoil);
protected:
	void UpdateRandomMeshTransformRecoil();
	void UpdateRandomCameraRotationRecoil();
	void ResetRecoilLocation(UTransformController* RecoilTC, FRecoilLocationParam& Param);
	void ResetRecoilRotation(UCharacterTransformController* RecoilTC, FRecoilRotationParam& Param);
	void UpRotationStabilization();
	void UpdateUpRotationParameters();
	void ResetUpRotationParameters();
	float GetRecoilCoef(UCurveFloat* Curve, float Recoil) const;
	void UpdateLocationRecoilParameters(float Recoil, FRecoilRandomParam& RandomParam, FSceneCompMoveParam& MoveParam, FSceneCompRotParam* RotParam = nullptr, float RotCoef = 0);
	void UpdateRotationRecoilParameters(float Recoil, FRecoilRandomParam& RandomParam, FSceneCompRotParam& RotParam);
	void UpdateHorizontalRecoilParameters(float HRecoil);
	void UpdateVerticalRecoilParameters(float VRecoil);
	//Fields
protected:

	UPROPERTY(EditDefaultsOnly)
		FRecoilCameraShakeParam AimingCameraShakeRecoil;
	UPROPERTY(EditDefaultsOnly)
		FRecoilLocationParam StandartMeshLocationRecoil;
	UPROPERTY(EditDefaultsOnly)
		FMeshRotationCoefsParam StandartMeshRotationCoefs;
	UPROPERTY(EditDefaultsOnly)
		FRecoilLocationParam AimingMeshLocationRecoil;
	
	UPROPERTY(EditDefaultsOnly)
		FRecoilRotationParam StandartCameraRotationRecoil;
	UPROPERTY(EditDefaultsOnly)
		FRecoilRotationParam AimingCameraRotationRecoil;

	UPROPERTY(EditDefaultsOnly)
		FRecoilCameraUpRotationParam StandartCameraUpRotation;
	UPROPERTY(EditDefaultsOnly)
		FRecoilCameraUpRotationParam AimingCameraUpRotation;

	UPROPERTY()
	UTransformController* MeshStandartRecoilTC;
	UPROPERTY()
	UTransformController* MeshAimingRecoilTC;

	UPROPERTY()
	UCharacterTransformController* CameraStandartRecoilTC;
	UPROPERTY()
	UCharacterTransformController* CameraAimingRecoilTC;
	UPROPERTY()
	UCharacterTransformController* CameraUpRotationTC;

	UPROPERTY()
	AFirstPersonPlayerController* FP_PlayerController;
	
	float VerticalRecoil;
	float HorizontalRecoil;

	bool bRecoilActivated = false;
	bool bIsAiming = false;

	bool CanCameraUpRotate = true;
	bool CanCameraStabilize = true;
};
