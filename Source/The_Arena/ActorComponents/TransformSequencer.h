// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"

#include "Curves/CurveVector.h"

#include "The_Arena/ActorComponents/TransformController.h"

#include "TransformSequencer.generated.h"


USTRUCT(BlueprintType)
struct FSceneCompMoveSeqParam
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LocProperties)
	FVector Location;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LocProperties)
	FVector ForwardSpeed;
};

USTRUCT(BlueprintType)
struct FSceneCompRotSeqParam
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RotProperties)
	FRotator Rotation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RotProperties)
	FVector ForwardSpeed;
};

DECLARE_MULTICAST_DELEGATE_OneParam(FUpdateTransformSignature, int);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class THE_ARENA_API UTransformSequencer : public UActorComponent
{
	GENERATED_BODY()
	//Methods
public:
	UFUNCTION(BlueprintNativeEvent)
	void OnBeginSequence();
	UFUNCTION(BlueprintNativeEvent)
	void OnEndSequence();

	UFUNCTION(BlueprintNativeEvent)
	void OnUpdateLocationIndexSequence(int Index);
	UFUNCTION(BlueprintNativeEvent)
	void OnUpdateRotationIndexSequence(int Index);

	UFUNCTION(BlueprintCallable)
	void SetSceneComponent(USceneComponent* Component);

	UFUNCTION(BlueprintCallable)
	void PlayLocationSequence();
	UFUNCTION(BlueprintCallable)
	void PlayLocationFragmentAt(int Index);
	UFUNCTION(BlueprintCallable)
	void PauseLocationSequence();
	UFUNCTION(BlueprintCallable)
	void StopLocationSequence();
	UFUNCTION(BlueprintCallable)
	void ResetLocationSequence();

	UFUNCTION(BlueprintCallable)
	void PlayRotationSequence();
	UFUNCTION(BlueprintCallable)
	void PlayRotationFragmentAt(int Index);
	UFUNCTION(BlueprintCallable)
	void PauseRotationSequence();
	UFUNCTION(BlueprintCallable)
	void StopRotationSequence();
	UFUNCTION(BlueprintCallable)
	void ResetRotationSequence();

	int GetCurrentLocationIndex() const;
	void SetCurrentLocationIndex(int Index);
	int GetCurrentRotationIndex() const;
	void SetCurrentRotationIndex(int Index);

	const TArray<FSceneCompMoveSeqParam>* GetLocationCoordinates() const;
	const TArray<FSceneCompRotSeqParam>* GetRotationCoordinates() const;

protected:	
	// Sets default values for this component's properties
	UTransformSequencer();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void UpdateLocationParams(int Index);
	void UpdateRotationParams(int Index);
	FVector CalculateForwardSpeedLocation(UCurveVector* CurForwardVectorCurve);
	FVector CalculateForwardSpeedRotation(UCurveVector* CurForwardVectorCurve);
	void CheckLocationForUpdate();
	void CheckRotationForUpdate();

	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
		TArray<FSceneCompMoveSeqParam> LocationCoordinates;
	UPROPERTY(EditDefaultsOnly)
		TArray<FSceneCompRotSeqParam> RotationCoordinates;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = LocProperties)
	UCurveVector* ForwardSpeedLocationCurve;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RotProperties)
	UCurveVector* ForwardSpeedRotationCurve;

	UPROPERTY(EditDefaultsOnly)
		float CurrencyForUpdateLocationIndex = 0.001f;
	UPROPERTY(EditDefaultsOnly)
		float CurrencyForUpdateRotationIndex = 0.001f;

	UTransformController* TransformController;
	
	bool CanChangeLocation = false;
	bool CanChangeRotation = false;
	int CurrentLocationIndex = 0;
	int CurrentRotationIndex = 0;
	//Event
public:
	FUpdateTransformSignature UpdateLocationDelegate;
	FUpdateTransformSignature UpdateRotationDelegate;
};
