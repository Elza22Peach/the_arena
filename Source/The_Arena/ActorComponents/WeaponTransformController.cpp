// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponTransformController.h"


UWeaponTransformController::UWeaponTransformController()
{

}

void UWeaponTransformController::SetScaleOfMoving(float Scale)
{
	ScaleOfMoving = Scale * MultiplyScaleOfMovingParameter;
}

void  UWeaponTransformController::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	SetRotationFromScale();
	SetLocationFromScale();
	UTransformController::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UWeaponTransformController::RotateSceneComponent(float DeltaTime, FSceneCompRotParam& Param, float& PassAngle, FRotator direction)
{
	auto ReturnToStartLambda = [&](int Koef) {
		float Offset = Koef * Param.BackSpeedRotation * DeltaTime * GetSpeedCoef(PassAngle, Param.BackSpeedRotationCurve);

		if (Koef * (PassAngle + Offset) > 0)
		{
			float DistanceToEndRotation = -PassAngle;

			if (SceneComp)
			{
				SceneComp->AddRelativeRotation(direction * DistanceToEndRotation);
				PassAngle = 0;
			}
		}
		else
		{
			if (SceneComp)
			{
				SceneComp->AddRelativeRotation(direction * Offset);
				PassAngle += Offset;
			}
		}
	};

	auto RotateComponentLambda = [&](int Koef, float Rotation) {
		float Offset = 0;

		if(Param.Rotation > 0)
		{
			if ((PassAngle > 0 && ScaleOfMoving < 0) || (PassAngle < 0 && ScaleOfMoving > 0))
			{
				Offset = Koef * Param.ForwardSpeedRotation * DeltaTime * fabsf(ScaleOfMoving);
			}
			else
			{
				Offset = Koef * Param.ForwardSpeedRotation * DeltaTime * fabsf(ScaleOfMoving) * GetSpeedCoef(PassAngle, Param.ForwardSpeedRotationCurve);
			}
		}
		else
		{
			if ((PassAngle < 0 && ScaleOfMoving < 0) || (PassAngle > 0 && ScaleOfMoving > 0))
			{
				Offset = Koef * Param.ForwardSpeedRotation * DeltaTime * fabsf(ScaleOfMoving);
			}
			else
			{
				Offset = Koef * Param.ForwardSpeedRotation * DeltaTime * fabsf(ScaleOfMoving) * GetSpeedCoef(PassAngle, Param.ForwardSpeedRotationCurve);
			}
		}

		if (Koef * (PassAngle + Offset) > Koef* Rotation)
		{
			float DistanceToEndRotation = Rotation - PassAngle;

			if (SceneComp)
			{
				SceneComp->AddRelativeRotation(direction * DistanceToEndRotation);
				PassAngle = Rotation;
			}
		}
		else
		{
			if (SceneComp)
			{
				SceneComp->AddRelativeRotation(direction * Offset);
				PassAngle += Offset;
			}
		}
	};

	if (FMath::IsNearlyZero(ScaleOfMoving, CurrencyScaleOfMoving))
	{
		if (PassAngle < 0)
		{
			ReturnToStartLambda(1);
		}
		else if (PassAngle > 0)
		{
			ReturnToStartLambda(-1);
		}
	}
	else if (ScaleOfMoving > 0)
	{
		if (PassAngle < Param.Rotation)
		{
			RotateComponentLambda(1, Param.Rotation);
		}
		else if (PassAngle > Param.Rotation)
		{
			RotateComponentLambda(-1, Param.Rotation);
		}
	}
	else
	{
		if (Param.Mirror)
		{
			if (PassAngle < -Param.Rotation)
			{
				RotateComponentLambda(1, -Param.Rotation);
			}
			else if (PassAngle > -Param.Rotation)
			{
				RotateComponentLambda(-1, -Param.Rotation);
			}
		}
		else
		{
			if (PassAngle < Param.Rotation)
			{
				RotateComponentLambda(1, Param.Rotation);
			}
			else if (PassAngle > Param.Rotation)
			{
				RotateComponentLambda(-1, Param.Rotation);
			}
		}
	}
}

void UWeaponTransformController::MoveSceneComponent(float DeltaTime, FSceneCompMoveParam& Param, float& PassDistance, FVector direction)
{
	auto ReturnToStartLambda = [&](int Koef) {

		float Offset = Koef * Param.BackSpeedLocation * DeltaTime * GetSpeedCoef(PassDistance, Param.BackSpeedLocationCurve);

		if (Koef * (PassDistance + Offset) > 0)
		{
			float DistanceToEndLocation = -PassDistance;

			if (SceneComp)
			{
				SceneComp->AddRelativeLocation(direction * DistanceToEndLocation);
				PassDistance = 0;
			}
		}
		else
		{
			if (SceneComp)
			{
				SceneComp->AddRelativeLocation(direction * Offset);
				PassDistance += Offset;
			}
		}
	};

	auto MoveComponentLambda = [&](int Koef, float Location) {
		float Offset = 0;

		if(Param.Location > 0)
		{
			if ((PassDistance > 0 && ScaleOfMoving < 0) || (PassDistance < 0 && ScaleOfMoving > 0))
			{
				Offset = Koef * Param.ForwardSpeedLocation * DeltaTime * fabsf(ScaleOfMoving);
			}
			else
			{
				Offset = Koef * Param.ForwardSpeedLocation * DeltaTime * fabsf(ScaleOfMoving) * GetSpeedCoef(PassDistance, Param.ForwardSpeedLocationCurve);
			}
		}
		else
		{
			if ((PassDistance < 0 && ScaleOfMoving < 0) || (PassDistance > 0 && ScaleOfMoving > 0))
			{
				Offset = Koef * Param.ForwardSpeedLocation * DeltaTime * fabsf(ScaleOfMoving);
			}
			else
			{
				Offset = Koef * Param.ForwardSpeedLocation * DeltaTime * fabsf(ScaleOfMoving) * GetSpeedCoef(PassDistance, Param.ForwardSpeedLocationCurve);
			}
		}
		
		if (Koef * (PassDistance + Offset) > Koef* Location)
		{
			float DistanceToEndLocation = Location - PassDistance;

			if (SceneComp)
			{
				SceneComp->AddRelativeLocation(direction * DistanceToEndLocation);
				PassDistance = Location;
			}
		}
		else
		{
			if (SceneComp)
			{
				SceneComp->AddRelativeLocation(direction * Offset);
				PassDistance += Offset;
			}
		}
	};

	if (FMath::IsNearlyZero(ScaleOfMoving, CurrencyScaleOfMoving))
	{
		if (PassDistance < 0)
		{
			ReturnToStartLambda(1);
		}
		else if (PassDistance > 0)
		{
			ReturnToStartLambda(-1);
		}
	}
	else if (ScaleOfMoving > 0)
	{
		if (PassDistance < Param.Location)
		{
			MoveComponentLambda(1, Param.Location);
		}
		else if (PassDistance > Param.Location)
		{
			MoveComponentLambda(-1, Param.Location);
		}
	}
	else
	{
		if (Param.Mirror)
		{
			if (PassDistance < -Param.Location)
			{
				MoveComponentLambda(1, -Param.Location);
			}
			else if (PassDistance > -Param.Location)
			{
				MoveComponentLambda(-1, -Param.Location);
			}
		}
		else
		{
			if (PassDistance < Param.Location)
			{
				MoveComponentLambda(1, Param.Location);
			}
			else if (PassDistance > Param.Location)
			{
				MoveComponentLambda(-1, Param.Location);
			}
		}
	}
}

void UWeaponTransformController::SetLocationFromScale()
{
	if(LocationFromScaleCurve)
	{
		auto LocationOnCurScale = LocationFromScaleCurve->GetVectorValue(GetScaleOfMoving());

		Forward.Location = LocationOnCurScale.X;
		Right.Location = LocationOnCurScale.Y;
		Up.Location = LocationOnCurScale.Z;
	}
}

void UWeaponTransformController::SetRotationFromScale()
{
	if (RotationFromScaleCurve)
	{
		auto RotationOnCurScale = RotationFromScaleCurve->GetVectorValue(GetScaleOfMoving());

		Roll.Rotation = RotationOnCurScale.X;
		Pitch.Rotation = RotationOnCurScale.Y;
		Yaw.Rotation = RotationOnCurScale.Z;
	}
		
}