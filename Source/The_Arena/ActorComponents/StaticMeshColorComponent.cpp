// Fill out your copyright notice in the Description page of Project Settings.


#include "StaticMeshColorComponent.h"

// Sets default values for this component's properties
UStaticMeshColorComponent::UStaticMeshColorComponent()
{
	// ...
}


// Called when the game starts
void UStaticMeshColorComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UStaticMeshColorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

FColor UStaticMeshColorComponent::GetStaticMeshColor() const
{
	return StaticMeshColor;
}

