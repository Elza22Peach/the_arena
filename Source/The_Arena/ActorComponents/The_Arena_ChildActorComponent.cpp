// Fill out your copyright notice in the Description page of Project Settings.


#include "The_Arena_ChildActorComponent.h"

#include "Net/UnrealNetwork.h"

void UThe_Arena_ChildActorComponent::CreateChildActor()
{
	Super::CreateChildActor();

	if(GetChildActor())
	{
		if(GetOwnerRole() == ROLE_Authority)
		{
			ChildActorPointer = GetChildActor();
			OnRep_ChildActorPointer();
		}
	}
}

AActor* UThe_Arena_ChildActorComponent::GetChildActorPointer() const
{
	return ChildActorPointer;
}

void UThe_Arena_ChildActorComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UThe_Arena_ChildActorComponent, ChildActorPointer);
}

void UThe_Arena_ChildActorComponent::OnRep_ChildActorPointer()
{
	ChildActorPointerChangedDispatcher.Broadcast();
}
