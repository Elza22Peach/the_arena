// Fill out your copyright notice in the Description page of Project Settings.


#include "TraderBasketComponent.h"

#include "The_Arena/ActorComponents/HealthComponent.h"


void UTraderBasketComponent::Initialization(UTraderInventoryComponent* InTraderInventoryComponent, UCustomInventoryComponent* InCustomInventoryComponent, UMoneyComponent* InMoneyComponent)
{
	if (!InTraderInventoryComponent || !InCustomInventoryComponent || !InMoneyComponent)
		return;

	TraderInventoryComponent = InTraderInventoryComponent;
	PlayerInventoryComponent = InCustomInventoryComponent;
	PlayerMoneyComponent = InMoneyComponent;	
}

void UTraderBasketComponent::DropItem(UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount)
{
	if (!DragAndDropComponent)
		return;

	FDraggedItem DraggedItem = DragAndDropComponent->GetDraggedItem();

	if(auto InventoryBaseComponent = DraggedItem.InventoryBaseComponent)
	{
		if(InventoryBaseComponent == this || InventoryBaseComponent == TraderInventoryComponent)
		{
			Super::DropItem(DragAndDropComponent, bWithItemAmount);
		}
	}
}

void UTraderBasketComponent::DropItemAt(int TopLeftIndex, UDragAndDropManagerComponent* DragAndDropComponent,
	bool bWithItemAmount)
{
	if (!DragAndDropComponent)
		return;

	FDraggedItem DraggedItem = DragAndDropComponent->GetDraggedItem();

	if (auto InventoryBaseComponent = DraggedItem.InventoryBaseComponent)
	{
		if (InventoryBaseComponent == this || InventoryBaseComponent == TraderInventoryComponent)
		{
			Super::DropItemAt(TopLeftIndex, DragAndDropComponent, bWithItemAmount);
		}
	}	
}

bool UTraderBasketComponent::CanUsed(AActor* ActorUser)
{
	if (!ActorUser)
		return false;

	if (auto Owner = GetOwner())
	{
		auto HealthComponent = ActorUser->FindComponentByClass<UHealthComponent>();

		if (HealthComponent)
		{
			if (!HealthComponent->IsAlive())
			{
				return false;
			}
		}

		float CurrentDistance = 0.0f;
		if (auto MeshComponent = Owner->FindComponentByClass<UMeshComponent>())
		{
			CurrentDistance = (MeshComponent->GetComponentLocation() - ActorUser->GetActorLocation()).Size();
		}
		else
		{
			CurrentDistance = Owner->GetDistanceTo(ActorUser);
		}

		if (CurrentDistance > InteractionDistance)
		{
			return false;
		}

		return true;
	}

	return false;
}

void UTraderBasketComponent::Buy()
{
	if (!PlayerInventoryComponent || !PlayerMoneyComponent || GetOwnerRole() != ROLE_Authority)
		return;

	auto PlayerMoney = PlayerMoneyComponent->GetMoney();
	
	if(PlayerMoney < CommonItemsCost)
	{
		return;
	}

	PlayerMoneyComponent->SetMoney(PlayerMoney - CommonItemsCost);
	
	auto AllItems = GetAllItems();

	for(auto Iter = AllItems.begin(); Iter != AllItems.end(); ++Iter)
	{
		if(auto ItemObject = Iter->Key)
		{	
			if(PlayerInventoryComponent->TryAddItem(ItemObject, true))
			{
				ItemObject->SetOwner(PlayerInventoryComponent->GetOwner());
				RemoveItem(ItemObject, ERemoveItemOption::WithSave);
			}
			else
			{
				RemoveItemFromBaseInventory(ItemObject);
			}
		}
	}
}
