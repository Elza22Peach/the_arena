// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerBasketComponent.h"

#include "The_Arena/ActorComponents/EquipmentComponent.h"


void UPlayerBasketComponent::Initialization(UCustomInventoryComponent* InCustomInventoryComponent, UEquipmentComponent* InEquipmentComponent, UMoneyComponent* InMoneyComponent)
{
	if (!InCustomInventoryComponent || !InEquipmentComponent)
		return;
	
	CustomInventoryComponent = InCustomInventoryComponent;
	EquipmentComponent = InEquipmentComponent;
	PlayerMoneyComponent = InMoneyComponent;
}

void UPlayerBasketComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if(GetOwnerRole() == ROLE_Authority)
	{
		auto AllItems = GetAllItems();

		for (auto Iter = AllItems.begin(); Iter != AllItems.end(); ++Iter)
		{
			auto ItemObject = (*Iter).Key;

			if(!CustomInventoryComponent || !CustomInventoryComponent->TryAddItem(ItemObject, true))
			{
				RemoveItemFromBaseInventory(ItemObject);
			}
		}

		Items.Empty();
	}
	
	Super::EndPlay(EndPlayReason);
}

void UPlayerBasketComponent::DropItem(UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount)
{
	if (!DragAndDropComponent)
		return;

	FDraggedItem DraggedItem = DragAndDropComponent->GetDraggedItem();

	if (auto InventoryBaseComponent = DraggedItem.InventoryBaseComponent)
	{
		if (InventoryBaseComponent == this || InventoryBaseComponent == CustomInventoryComponent || InventoryBaseComponent == EquipmentComponent)
		{
			Super::DropItem(DragAndDropComponent, bWithItemAmount);
		}
	}
}

void UPlayerBasketComponent::DropItemAt(int TopLeftIndex, UDragAndDropManagerComponent* DragAndDropComponent,
	bool bWithItemAmount)
{
	if (!DragAndDropComponent)
		return;

	FDraggedItem DraggedItem = DragAndDropComponent->GetDraggedItem();

	if (auto InventoryBaseComponent = DraggedItem.InventoryBaseComponent)
	{
		if (InventoryBaseComponent == this || InventoryBaseComponent == CustomInventoryComponent || InventoryBaseComponent == EquipmentComponent)
		{
			Super::DropItemAt(TopLeftIndex, DragAndDropComponent, bWithItemAmount);
		}
	}
}

void UPlayerBasketComponent::Sell()
{
	if (!PlayerMoneyComponent)
		return;

	PlayerMoneyComponent->SetMoney(PlayerMoneyComponent->GetMoney() + CommonItemsCost);

	auto AllItems = GetAllItems();
	
	for(auto Iter = AllItems.begin(); Iter != AllItems.end(); ++Iter)
	{
		if(auto ItemObject = Iter->Key)
		{
			RemoveItem(ItemObject, ERemoveItemOption::WithoutSave);
		}
	}
}

void UPlayerBasketComponent::UpdateCommonItemsCost()
{
	if (GetOwnerRole() != ROLE_Authority)
		return;

	auto ItemsLocal = GetAllItems();

	float CommonItemsCostLocal = 0.0f;

	for (auto ItemsIterator = ItemsLocal.begin(); ItemsIterator != ItemsLocal.end(); ++ItemsIterator)
	{
		if (auto ItemObject = ItemsIterator->Key)
		{
			CommonItemsCostLocal += ItemObject->GetCurrentItemCost();
		}
	}

	if(CommonItemsCostLocal > 0)
	{
		CommonItemsCost = CommonItemsCostLocal / 2;
	}
	else
	{
		CommonItemsCost = 0;
	}

	OnRep_CommonItemsCost();
}
