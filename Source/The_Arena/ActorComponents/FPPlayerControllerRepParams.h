// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "FPPlayerControllerRepParams.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THE_ARENA_API UFPPlayerControllerRepParams : public UActorComponent
{
	GENERATED_BODY()

		//Methods
protected:
	UFPPlayerControllerRepParams();
	void BeginPlay() override;
	UFUNCTION()
	void Initialization();
	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	UFUNCTION(BlueprintCallable)
	void SetHoldingToCrouch(bool bInHoldingToCrouch);
	UFUNCTION(BlueprintCallable)
	void SetHoldingToAim(bool bInHoldingToAim);
	UFUNCTION(BlueprintCallable)
	void SetHoldingToSlowWalk(bool bInHoldingToSlowWalk);
	UFUNCTION(BlueprintCallable)
	void SetHoldingToRun(bool bInHoldingToRun);
public:
	UFUNCTION(BlueprintCallable)
	bool IsHoldingToCrouch() const;
	UFUNCTION(BlueprintCallable)
	bool IsHoldingToAim() const;
	UFUNCTION(BlueprintCallable)
	bool IsHoldingToSlowWalk() const;
	UFUNCTION(BlueprintCallable)
	bool IsHoldingToRun() const;
	
	//Fields
protected:
	UPROPERTY(Replicated)
	bool bHoldingToCrouch = false;
	UPROPERTY(Replicated)
	bool bHoldingToAim = true;
	UPROPERTY(Replicated)
	bool bHoldingToSlowWalk = true;
	UPROPERTY(Replicated)
	bool bHoldingToRun = true;
	UPROPERTY(Replicated)
	bool bInitialized = false;
};
