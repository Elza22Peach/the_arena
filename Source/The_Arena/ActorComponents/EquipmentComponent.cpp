// Fill out your copyright notice in the Description page of Project Settings.


#include "EquipmentComponent.h"
#include "Net/UnrealNetwork.h"

#include "The_Arena/Actors/PersonalEquipment/Outfit.h"
#include "The_Arena/Actors/PersonalEquipment/AdditionalEquipment.h"
#include "The_Arena/Actors/Weapons/RifleBase.h"
#include "The_Arena/UObjects/DataAssets/FiringWeaponCommonParams.h"
#include "The_Arena/GameStates/CustomGameState.h"
#include "The_Arena/ActorComponents/CustomInventoryComponent.h"
#include "The_Arena/ActorComponents/PlayerBasketComponent.h"
#include "The_Arena/Actors/InventoryItemObjects/PersonalEquipment/OutfitItemObject.h"


bool operator==(const FEquipment& First, const FEquipment& Second)
{
	return First.InventoryItemObject == Second.InventoryItemObject && First.InventoryItem == Second.InventoryItem;
}

// Sets default values for this component's properties
UEquipmentComponent::UEquipmentComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UEquipmentComponent::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UEquipmentComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UEquipmentComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UEquipmentComponent, UsingItemInHands);
	DOREPLIFETIME(UEquipmentComponent, UsingItemObjectInHands);
	DOREPLIFETIME(UEquipmentComponent, BackgroundUsingItem);
	DOREPLIFETIME(UEquipmentComponent, AdditiveUsingItem);
	DOREPLIFETIME(UEquipmentComponent, Outfit);
	DOREPLIFETIME(UEquipmentComponent, AdditionalEquipment);
	DOREPLIFETIME(UEquipmentComponent, FirstFiringWeapon);
	DOREPLIFETIME(UEquipmentComponent, SecondFiringWeapon);
	DOREPLIFETIME(UEquipmentComponent, Pistol);
	DOREPLIFETIME(UEquipmentComponent, Melee);
}

void UEquipmentComponent::OnRep_UsingItemInHands()
{
	if (auto InHandsUsableItem = Cast<IInHandsUsable>(UsingItemInHands))
	{
		InHandsUsableItem->Execute_Use(UsingItemInHands);
	}

	UsingItemInHandsChangedDispatcher.Broadcast();
}

void UEquipmentComponent::OnRep_Outfit()
{
	OutfitChangedDispatcher.Broadcast();
}

void UEquipmentComponent::OnRep_AdditionalEquipment()
{
	AdditionalEquipmentChangedDispatcher.Broadcast();
}

void UEquipmentComponent::OnRep_FirstFiringWeapon()
{
	FirstFiringWeaponChangedDispatcher.Broadcast();
}

void UEquipmentComponent::OnRep_SecondFiringWeapon()
{
	SecondFiringWeaponChangedDispatcher.Broadcast();
}

void UEquipmentComponent::OnRep_Pistol()
{
	PistolChangedDispatcher.Broadcast();
}

void UEquipmentComponent::OnRep_Melee()
{
	MeleeChangedDispatcher.Broadcast();
}

void UEquipmentComponent::UseItemInHands(AInventoryItem* InventoryItem, AInventoryItemObject* InventoryItemObject)
{
	auto UseItemInHandsLambda = [&](AInventoryItem* Item, AInventoryItemObject* ItemObject)
	{
		if(Item)
		{
			UseItemInHands(Item, ItemObject);
		}		
	};
	
	if (!InventoryItem || !InventoryItemObject || InventoryItem->IsPendingKillPending())
		return;

	if (Cast<IInHandsUsable>(InventoryItem))
	{
		if (UsingItemInHands)
		{
			if (InventoryItem == UsingItemInHands)
			{
				RemoveItemFromHands();
			}
			else
			{
				RemoveItemFromHands();
				UseItemInHandsOnEndRemovingDelegate.BindLambda(UseItemInHandsLambda, InventoryItem, InventoryItemObject);
			}
		}
		else
		{
			SetUsingItemInHands(InventoryItem);
			SetUsingItemObjectInHands(InventoryItemObject);
		}
	}
}

void UEquipmentComponent::RemoveItemFromHands(bool bWithDestroyItem)
{
	if (auto InHandsUsableItem = Cast<IInHandsUsable>(UsingItemInHands))
	{
		if (!bItemRemoving)
		{
			InHandsUsableItem->Execute_Remove(UsingItemInHands);
			InHandsUsableItem->Execute_BindToRemovedDispatcher(UsingItemInHands, this, "OnEndRemovingItemFromHands");
			bItemRemoving = true;
			bDestroyItemInHands = bWithDestroyItem;
		}
	}
}

void UEquipmentComponent::OnEndRemovingItemFromHands()
{
	if (!UsingItemInHands)
		return;

	if (auto InHandsUsableItem = Cast<IInHandsUsable>(UsingItemInHands))
	{
		InHandsUsableItem->Execute_UnbindFromRemovedDispatcher(UsingItemInHands, this, "OnEndRemovingItemFromHands");

		SetUsingItemInHands(nullptr);
		SetUsingItemObjectInHands(nullptr);
		
		bItemRemoving = false;
		
		if (bDestroyItemInHands)
		{
			if(DestroyItemInHandsDelegate.IsBound())
			{
				DestroyItemInHandsDelegate.Execute();
				DestroyItemInHandsDelegate.Unbind();
			}
			
			bDestroyItemInHands = false;
		}
	}

	EndRemovingItemFromHandsDispatcher.Broadcast();

	if (UseItemInHandsOnEndRemovingDelegate.IsBound())
	{
		UseItemInHandsOnEndRemovingDelegate.Execute();
		UseItemInHandsOnEndRemovingDelegate.Unbind();
	}
}

void UEquipmentComponent::UseItemOnBackground(AInventoryItem* InventoryItem)
{

}

void UEquipmentComponent::UseItemAdditive(AInventoryItem* InventoryItem)
{

}

FDMD& UEquipmentComponent::GetOutfitChangedDispatcher()
{
	return OutfitChangedDispatcher;
}

FDMD& UEquipmentComponent::GetAdditionalEquipmentChangedDispatcher()
{
	return AdditionalEquipmentChangedDispatcher;
}

FDMD& UEquipmentComponent::GetFirstFiringWeaponChangedDispatcher()
{
	return FirstFiringWeaponChangedDispatcher;
}

FDMD& UEquipmentComponent::GetSecondFiringWeaponChangedDispatcher()
{
	return SecondFiringWeaponChangedDispatcher;
}

FDMD& UEquipmentComponent::GetPistolChangedDispatcher()
{
	return PistolChangedDispatcher;
}

FDMD& UEquipmentComponent::GetMeleeChangedDispatcher()
{
	return MeleeChangedDispatcher;
}

FDMD& UEquipmentComponent::GetEndRemovingItemFromHandsDispatcher()
{
	return EndRemovingItemFromHandsDispatcher;
}

AInventoryItem* UEquipmentComponent::GetUsingItemInHands() const
{
	return UsingItemInHands;
}

AInventoryItemObject* UEquipmentComponent::GetUsingItemObjectInHands() const
{
	return UsingItemObjectInHands;
}

void UEquipmentComponent::SetUsingItemInHands(AInventoryItem* InUsingItemInHands)
{
	UsingItemInHands = InUsingItemInHands;

	if (GetOwnerRole() == ROLE_Authority)
	{
		OnRep_UsingItemInHands();
	}
}

void UEquipmentComponent::SetUsingItemObjectInHands(AInventoryItemObject* InUsingItemObjectInHands)
{
	UsingItemObjectInHands = InUsingItemObjectInHands;
}

AInventoryItem* UEquipmentComponent::GetBackgroundUsingItem() const
{
	return BackgroundUsingItem;
}

void UEquipmentComponent::SetBackgroundUsingItem(AInventoryItem* InBackgroundUsingItem)
{
	BackgroundUsingItem = InBackgroundUsingItem;
}

AInventoryItem* UEquipmentComponent::GetAdditiveUsingItem() const
{
	return AdditiveUsingItem;
}

void UEquipmentComponent::SetAdditiveUsingItem(AInventoryItem* InAdditiveUsingItem)
{
	AdditiveUsingItem = InAdditiveUsingItem;
}

FEquipment UEquipmentComponent::GetEquipment(EEquipmentType EquipmentType) const
{
	switch (EquipmentType)
	{
	case EEquipmentType::Outfit:
		return Outfit;
	case EEquipmentType::AdditionalEquipment:
		return AdditionalEquipment;
	case EEquipmentType::FirstFiringWeapon:
		return FirstFiringWeapon;
	case EEquipmentType::SecondFiringWeapon:
		return SecondFiringWeapon;
	case EEquipmentType::Pistol:
		return Pistol;
	case EEquipmentType::Melee:
		return Melee;
	default:
		FEquipment Equipment = { nullptr, nullptr };
		return Equipment;
	}
}

EEquipmentType UEquipmentComponent::FindEquipmentType(AInventoryItemObject* InventoryItemObject)
{
	if (!InventoryItemObject)
		return EEquipmentType::NONE;

	EEquipmentType EquipmentType = EEquipmentType::NONE;
	
	if(InventoryItemObject == Outfit.InventoryItemObject)
	{
		EquipmentType = EEquipmentType::Outfit;
	}
	else if(InventoryItemObject == AdditionalEquipment.InventoryItemObject)
	{
		EquipmentType = EEquipmentType::AdditionalEquipment;
	}
	else if (InventoryItemObject == FirstFiringWeapon.InventoryItemObject)
	{
		EquipmentType = EEquipmentType::FirstFiringWeapon;
	}
	else if (InventoryItemObject == SecondFiringWeapon.InventoryItemObject)
	{
		EquipmentType = EEquipmentType::SecondFiringWeapon;
	}
	else if (InventoryItemObject == Pistol.InventoryItemObject)
	{
		EquipmentType = EEquipmentType::Pistol;
	}
	else if (InventoryItemObject == Melee.InventoryItemObject)
	{
		EquipmentType = EEquipmentType::Melee;
	}

	return EquipmentType;
}

void UEquipmentComponent::RemoveItemFromBaseInventory(AInventoryItemObject* InventoryItemObject)
{
	if (!InventoryItemObject)
		return;

	auto CustomGameState = GetWorld()->GetGameState<ACustomGameState>();
	auto Equipment = FindEquipmentType(InventoryItemObject);
	
	if (CustomGameState && Equipment != EEquipmentType::NONE)
	{
		if(auto DroppedItemsManager = CustomGameState->GetDroppedItemsManager())
		{
			DroppedItemsManager->SpawnInventoryItemInFrontOfTargetActor(InventoryItemObject, GetOwner());
			RemoveEquipment(Equipment, ERemoveItemOption::WithoutSave);
		}	
	}
}

void UEquipmentComponent::DragItem(AInventoryItemObject* InventoryItemObject,
	UDragAndDropManagerComponent* DragAndDropComponent)
{
	if (!InventoryItemObject || !DragAndDropComponent)
		return;

	auto EquipmentType = FindEquipmentType(InventoryItemObject);

	if (EquipmentType != EEquipmentType::NONE)
	{
		const int Tile = static_cast<int>(EquipmentType);
		DragAndDropComponent->SetDraggedItem(InventoryItemObject, this, Tile);
		RemoveEquipment(EquipmentType, ERemoveItemOption::WithSave);
	}
}

void UEquipmentComponent::DropItemAt(int TileIndex, UDragAndDropManagerComponent* DragAndDropComponent, bool bWithItemAmount)
{
	if (!DragAndDropComponent)
		return;
	
	EEquipmentType EquipmentType = static_cast<EEquipmentType>(TileIndex);

	auto DraggedItem = DragAndDropComponent->GetDraggedItem();

	if(!Cast<UCustomInventoryComponent>(DraggedItem.InventoryBaseComponent) &&
		!Cast<UPlayerBasketComponent>(DraggedItem.InventoryBaseComponent))
	{
		return;
	}

	if (auto DraggedInventoryItemObject = DraggedItem.InventoryItemObject)
	{
		auto Equipment = CreateEquipment(DraggedInventoryItemObject, EquipmentType, true);
		
		if (Equipment.InventoryItemObject)
		{
			DragAndDropComponent->SetDraggedItem(nullptr, nullptr);
		}
	}
}

FEquipment UEquipmentComponent::CreateEquipment(AInventoryItemObject* InventoryItemObject, EEquipmentType EquipmentType, bool WithSaveUsingItem)
{
	FEquipment OutEquipment = { nullptr, nullptr };
	
	if (!InventoryItemObject)
		return OutEquipment;

	switch (EquipmentType)
	{
	case EEquipmentType::NONE:
		break;
	case EEquipmentType::Outfit:
		OutEquipment = CreateOutfit(InventoryItemObject, WithSaveUsingItem);
		break;
	case EEquipmentType::AdditionalEquipment:
		OutEquipment = CreateAdditionalEquipment(InventoryItemObject, WithSaveUsingItem);
		break;
	case EEquipmentType::FirstFiringWeapon:
		OutEquipment = CreateFirstFiringWeapon(InventoryItemObject, WithSaveUsingItem);
		break;
	case EEquipmentType::SecondFiringWeapon:
		OutEquipment = CreateSecondFiringWeapon(InventoryItemObject, WithSaveUsingItem);
		break;
	case EEquipmentType::Pistol:
		OutEquipment = CreatePistol(InventoryItemObject, WithSaveUsingItem);
		break;
	case EEquipmentType::Melee:
		OutEquipment = CreateMelee(InventoryItemObject, WithSaveUsingItem);
		break;
	default:
		break;
	}

	return OutEquipment;
}

void UEquipmentComponent::RemoveEquipment(EEquipmentType EquipmentType, ERemoveItemOption RemoveItemOption)
{
	switch (EquipmentType)
	{
		case EEquipmentType::NONE:
			break;
		case EEquipmentType::Outfit:
			RemoveOutfit(RemoveItemOption);
			break;
		case EEquipmentType::AdditionalEquipment:
			RemoveAdditionalEquipment(RemoveItemOption);
			break;
		case EEquipmentType::FirstFiringWeapon:
			RemoveFirstFiringWeapon(RemoveItemOption);
			break;
		case EEquipmentType::SecondFiringWeapon:
			RemoveSecondFiringWeapon(RemoveItemOption);
			break;
		case EEquipmentType::Pistol:
			RemovePistol(RemoveItemOption);
			break;
		case EEquipmentType::Melee:
			RemoveMelee(RemoveItemOption);
			break;
		default:
			break;
	}
}

void UEquipmentComponent::RemoveEquipmentWithAddedToInventory(EEquipmentType EquipmentType)
{
	auto EquipmentItemObject = GetEquipment(EquipmentType).InventoryItemObject;
	auto Owner = GetOwner();
	
	if (EquipmentItemObject && Owner)
	{
		if(auto InventoryComponent = Owner->FindComponentByClass<UCustomInventoryComponent>())
		{
			if (!InventoryComponent->TryAddItem(EquipmentItemObject, false))
			{
				RemoveItemFromBaseInventory(EquipmentItemObject);
				return;
			}
		}
	}

	RemoveEquipment(EquipmentType, ERemoveItemOption::WithSave);
}

void UEquipmentComponent::SetEquipment(EEquipmentType EquipmentType, const FEquipment& InEquipment)
{
	switch (EquipmentType)
	{
	case EEquipmentType::Outfit:
	{
		Outfit = InEquipment;

		if (GetOwnerRole() == ROLE_Authority)
		{
			OnRep_Outfit();
		}

		break;
	}
	case EEquipmentType::AdditionalEquipment:
	{
		AdditionalEquipment = InEquipment;

		if (GetOwnerRole() == ROLE_Authority)
		{
			OnRep_AdditionalEquipment();
		}

		break;
	}
	case EEquipmentType::FirstFiringWeapon:
	{
		FirstFiringWeapon = InEquipment;

		if (GetOwnerRole() == ROLE_Authority)
		{
			OnRep_FirstFiringWeapon();
		}

		break;
	}
	case EEquipmentType::SecondFiringWeapon:
	{
		SecondFiringWeapon = InEquipment;

		if (GetOwnerRole() == ROLE_Authority)
		{
			OnRep_SecondFiringWeapon();
		}

		break;
	}
	case EEquipmentType::Pistol:
	{
		Pistol = InEquipment;

		if (GetOwnerRole() == ROLE_Authority)
		{
			OnRep_Pistol();
		}

		break;
	}
	case EEquipmentType::Melee:
	{
		Melee = InEquipment;

		if (GetOwnerRole() == ROLE_Authority)
		{
			OnRep_Melee();
		}

		break;
	}
	default:
		break;
	}
}

FEquipment UEquipmentComponent::CreateOutfit(AInventoryItemObject* InventoryItemObject, bool WithSaveUsingItem)
{
	FEquipment OutEquipment = { nullptr, nullptr };
	
	if (!InventoryItemObject)
		return OutEquipment;

	if(auto OutfitItemObject = Cast<AOutfitItemObject>(InventoryItemObject))
	{
		auto ItemCommonParams = OutfitItemObject->GetInventoryItemCommonParams();

		if (ItemCommonParams)
		{
			if (Cast<AOutfit>(ItemCommonParams->GetClassOfInventoryItem().GetDefaultObject()))
			{
				OutEquipment = CreateEquipmentCommon(EEquipmentType::Outfit, InventoryItemObject, WithSaveUsingItem);

				if (auto OutfitItem = Cast<AOutfit>(OutEquipment.InventoryItem))
				{
					OutfitItem->SetPhysicalProtection(OutfitItemObject->GetPhysicalProtection());
					OutfitItem->OutfitInitialization();
				}
			}
		}
	}

	return OutEquipment;
}

void UEquipmentComponent::RemoveOutfit(ERemoveItemOption RemoveItemOption)
{
	if(auto OutfitItem = Cast<AOutfit>(Outfit.InventoryItem))
	{
		OutfitItem->OutfitDeinitialization();
	}
	
	RemoveEquipmentCommon(EEquipmentType::Outfit, RemoveItemOption);
}

FEquipment UEquipmentComponent::CreateAdditionalEquipment(AInventoryItemObject* InventoryItemObject, bool WithSaveUsingItem)
{
	FEquipment OutEquipment = { nullptr, nullptr };
	
	if (!InventoryItemObject)
		return OutEquipment;

	auto ItemCommonParams = InventoryItemObject->GetInventoryItemCommonParams();

	if (ItemCommonParams)
	{
		if (Cast<AAdditionalEquipment>(ItemCommonParams->GetClassOfInventoryItem().GetDefaultObject()))
		{
			OutEquipment = CreateEquipmentCommon(EEquipmentType::AdditionalEquipment, InventoryItemObject, WithSaveUsingItem);
		}
	}

	return OutEquipment;
}

void UEquipmentComponent::RemoveAdditionalEquipment(ERemoveItemOption RemoveItemOption)
{
	RemoveEquipmentCommon(EEquipmentType::AdditionalEquipment, RemoveItemOption);
}

FEquipment UEquipmentComponent::CreateFirstFiringWeapon(AInventoryItemObject* InventoryItemObject, bool WithSaveUsingItem)
{
	FEquipment OutEquipment = { nullptr, nullptr };

	if (!InventoryItemObject)
		return OutEquipment;

	auto ItemCommonParams = Cast<UFiringWeaponCommonParams>(InventoryItemObject->GetInventoryItemCommonParams());

	if (ItemCommonParams)
	{
		if (Cast<ARifleBase>(ItemCommonParams->GetClassOfInventoryItem().GetDefaultObject()))
		{
			if (ItemCommonParams->GetFiringWeaponType() != EFiringWeaponType::Pistol)
			{
				if(UsingItemInHands && UsingItemInHands == FirstFiringWeapon.InventoryItem)
				{
					return OutEquipment;
				}
				
				OutEquipment = CreateEquipmentCommon(EEquipmentType::FirstFiringWeapon, InventoryItemObject, WithSaveUsingItem);

				if (auto Item = Cast<ARifleBase>(OutEquipment.InventoryItem))
				{
					Item->MulticastOnBackInitialization(EEquipmentType::FirstFiringWeapon);
					
					if (UsingItemInHands == nullptr)
					{
						ServerUseEquipment(EEquipmentType::FirstFiringWeapon);
					}
				}
			}
		}
	}

	return OutEquipment;
}

void UEquipmentComponent::RemoveFirstFiringWeapon(ERemoveItemOption RemoveItemOption)
{
	auto RemoveFirstFiringWeaponLambda = [&]()
	{
		RemoveFirstFiringWeapon(RemoveItemOption);
	};
	
	if (UsingItemInHands && UsingItemInHands == FirstFiringWeapon.InventoryItem)
	{
		if(bItemRemoving)
		{
			bDestroyItemInHands = true;
		}
		else
		{
			RemoveItemFromHands(true);
		}
		
		DestroyItemInHandsDelegate.BindLambda(RemoveFirstFiringWeaponLambda);
	}
	else
	{
		RemoveEquipmentCommon(EEquipmentType::FirstFiringWeapon, RemoveItemOption);
	}	
}

FEquipment UEquipmentComponent::CreateSecondFiringWeapon(AInventoryItemObject* InventoryItemObject, bool WithSaveUsingItem)
{
	FEquipment OutEquipment = { nullptr, nullptr };

	if (!InventoryItemObject)
		return OutEquipment;

	auto ItemCommonParams = Cast<UFiringWeaponCommonParams>(InventoryItemObject->GetInventoryItemCommonParams());

	if (ItemCommonParams)
	{
		if (Cast<ARifleBase>(ItemCommonParams->GetClassOfInventoryItem().GetDefaultObject()))
		{
			if (ItemCommonParams->GetFiringWeaponType() != EFiringWeaponType::Pistol)
			{
				if (UsingItemInHands && UsingItemInHands == SecondFiringWeapon.InventoryItem)
				{
					return OutEquipment;
				}
				
				OutEquipment = CreateEquipmentCommon(EEquipmentType::SecondFiringWeapon, InventoryItemObject, WithSaveUsingItem);

				if (auto Item = Cast<ARifleBase>(OutEquipment.InventoryItem))
				{
					Item->MulticastOnBackInitialization(EEquipmentType::SecondFiringWeapon);	

					if (UsingItemInHands == nullptr)
					{
						ServerUseEquipment(EEquipmentType::SecondFiringWeapon);
					}
				}
			}
		}
	}

	return OutEquipment;
}

void UEquipmentComponent::RemoveSecondFiringWeapon(ERemoveItemOption RemoveItemOption)
{
	auto RemoveSecondFiringWeaponLambda = [&]()
	{
		RemoveSecondFiringWeapon(RemoveItemOption);
	};

	if (UsingItemInHands && UsingItemInHands == SecondFiringWeapon.InventoryItem)
	{
		if (bItemRemoving)
		{
			bDestroyItemInHands = true;
		}
		else
		{
			RemoveItemFromHands(true);
		}
		
		DestroyItemInHandsDelegate.BindLambda(RemoveSecondFiringWeaponLambda);
	}
	else
	{
		RemoveEquipmentCommon(EEquipmentType::SecondFiringWeapon, RemoveItemOption);
	}
}

FEquipment UEquipmentComponent::CreatePistol(AInventoryItemObject* InventoryItemObject, bool WithSaveUsingItem)
{
	FEquipment OutEquipment = { nullptr, nullptr };

	if (!InventoryItemObject)
		return OutEquipment;

	auto ItemCommonParams = Cast<UFiringWeaponCommonParams>(InventoryItemObject->GetInventoryItemCommonParams());

	if (ItemCommonParams)
	{
		if (Cast<ARifleBase>(ItemCommonParams->GetClassOfInventoryItem().GetDefaultObject()))
		{
			if (ItemCommonParams->GetFiringWeaponType() == EFiringWeaponType::Pistol)
			{
				if (UsingItemInHands && UsingItemInHands == Pistol.InventoryItem)
				{
					return OutEquipment;
				}

				OutEquipment = CreateEquipmentCommon(EEquipmentType::Pistol, InventoryItemObject, WithSaveUsingItem);

				if (auto ItemObject = OutEquipment.InventoryItemObject)
				{
					if (ItemObject->IsRotated())
					{
						ItemObject->ServerRotate();
					}
				}

				if (auto Item = Cast<ARifleBase>(OutEquipment.InventoryItem))
				{
					Item->MulticastOnBackInitialization(EEquipmentType::Pistol);

					if (UsingItemInHands == nullptr)
					{
						ServerUseEquipment(EEquipmentType::Pistol);
					}
				}
			}
		}
	}

	return OutEquipment;
}

void UEquipmentComponent::RemovePistol(ERemoveItemOption RemoveItemOption)
{
	auto RemovePistolLambda = [&]()
	{
		RemovePistol(RemoveItemOption);
	};

	if (UsingItemInHands && UsingItemInHands == Pistol.InventoryItem)
	{
		if (bItemRemoving)
		{
			bDestroyItemInHands = true;
		}
		else
		{
			RemoveItemFromHands(true);
		}

		DestroyItemInHandsDelegate.BindLambda(RemovePistolLambda);
	}
	else
	{
		RemoveEquipmentCommon(EEquipmentType::Pistol, RemoveItemOption);
	}
}

FEquipment UEquipmentComponent::CreateMelee(AInventoryItemObject* InventoryItemObject, bool WithSaveUsingItem)
{
	FEquipment OutEquipment = { nullptr, nullptr };

	return OutEquipment;
}

void UEquipmentComponent::RemoveMelee(ERemoveItemOption RemoveItemOption)
{
	
}

FEquipment UEquipmentComponent::CreateEquipmentCommon(EEquipmentType EquipmentType, AInventoryItemObject* InventoryItemObject, bool WithSaveUsingItem)
{
	FEquipment OutEquipment = {nullptr, nullptr};
	
	if (!InventoryItemObject)
	{
		return OutEquipment;
	}

	if (auto ItemCommonParams = InventoryItemObject->GetInventoryItemCommonParams())
	{
		if(auto CustomGameState = Cast<ACustomGameState>(GetWorld()->GetGameState()))
		{
			if(auto InventoryItemsManager = CustomGameState->GetInventoryItemsManager())
			{
				FActorSpawnParameters SpawnParameters;
				SpawnParameters.Owner = GetOwner();
				
				FInventoryItemParams InventoryItemParams;
				InventoryItemParams.ItemObject = InventoryItemObject;
				
				if (auto EquipmentItem = InventoryItemsManager->CreateInventoryItem(ItemCommonParams->GetClassOfInventoryItem(), SpawnParameters, InventoryItemParams))
				{
					if (WithSaveUsingItem)
					{
						RemoveEquipmentWithAddedToInventory(EquipmentType);
					}
					else
					{
						RemoveEquipment(EquipmentType, ERemoveItemOption::WithoutSave);
					}

					if (InventoryItemObject->IsRotated())
					{
						InventoryItemObject->ServerRotate();
					}

					OutEquipment.InventoryItemObject = InventoryItemObject;
					OutEquipment.InventoryItem = EquipmentItem;

					SetEquipment(EquipmentType, OutEquipment);
				}
			}
		}
	}

	return OutEquipment;
}

void UEquipmentComponent::RemoveEquipmentCommon(EEquipmentType EquipmentType, ERemoveItemOption RemoveItemOption)
{
	auto Equipment = GetEquipment(EquipmentType);
	
	if (auto EquipmentItemObject = Equipment.InventoryItemObject)
	{
		if (RemoveItemOption == ERemoveItemOption::WithoutSave)
		{
			EquipmentItemObject->Destroy();
		}
	}

	if (auto EquipmentItem = Equipment.InventoryItem)
	{
		EquipmentItem->Destroy();
	}

	SetEquipment(EquipmentType, { nullptr, nullptr });
}

FDMD& UEquipmentComponent::GetUsingItemInHandsChangedDispatcher()
{
	return UsingItemInHandsChangedDispatcher;
}

void UEquipmentComponent::ServerUseEquipment_Implementation(EEquipmentType EquipmentType)
{
	FEquipment Equipment;

	switch (EquipmentType)
	{
	case EEquipmentType::FirstFiringWeapon:
		Equipment = FirstFiringWeapon;
		break;
	case EEquipmentType::SecondFiringWeapon:
		Equipment = SecondFiringWeapon;
		break;
	case EEquipmentType::Pistol:
		Equipment = Pistol;
		break;
	case EEquipmentType::Melee:
		Equipment = Melee;
		break;
	default:
		return;
	}

	if (Cast<IInHandsUsable>(Equipment.InventoryItem))
	{
		UseItemInHands(Equipment.InventoryItem, Equipment.InventoryItemObject);
	}
}