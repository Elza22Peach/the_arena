// Fill out your copyright notice in the Description page of Project Settings.


#include "AccuringEffectComponent.h"
#include "Net/UnrealNetwork.h"

#include "The_Arena/ActorComponents/HealthComponent.h"


void UAccuringEffectComponent::AccuringEffectInitialization(float InMaxAmountEffectPerSec, float InAmountEffectPerSec,
	TSubclassOf<UStatBaseComponent> InStatComponentClass, FCharacteristicEffectParams InEffectParams)
{
	MaxAmountEffectPerSec = InMaxAmountEffectPerSec;
	StandartEffectBaseOnStatInitialization(InAmountEffectPerSec, InStatComponentClass, InEffectParams);
}

float UAccuringEffectComponent::GetCurrentAmountEffectPerSec() const
{
	return CurrentAmountEffectPerSec;
}

void UAccuringEffectComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UAccuringEffectComponent, CurrentAmountEffectPerSec);
}

void UAccuringEffectComponent::CreateEffectInternal()
{
	CurrentAmountEffectPerSecInitialization();

	Super::CreateEffectInternal();
}

void UAccuringEffectComponent::CurrentAmountEffectPerSecInitialization()
{
	CurrentAmountEffectPerSec = AmountEffectPerSec;

	if (auto Owner = GetOwner())
	{
		TArray<UAccuringEffectComponent*> AccuringEffects;
		Owner->GetComponents<UAccuringEffectComponent>(AccuringEffects);

		for (auto AccuringEffect : AccuringEffects)
		{
			if(AccuringEffect)
			{
				if (AccuringEffect->GetClass() == GetClass() && AccuringEffect != this)
				{
					CurrentAmountEffectPerSec += AccuringEffect->GetCurrentAmountEffectPerSec();

					if (FMath::Abs(CurrentAmountEffectPerSec) > FMath::Abs(MaxAmountEffectPerSec))
					{
						CurrentAmountEffectPerSec = MaxAmountEffectPerSec;
					}

					AccuringEffect->RemoveEffect();
					break;
				}
			}	
		}
	}
}

void UAccuringEffectComponent::CreateStandartEffect()
{
	if (GetOwnerRole() != ROLE_Authority)
		return;

	if (StatComponent)
	{
		StatComponent->AddBonusEffect(EffectParams.EffectName, CurrentAmountEffectPerSec);
	}
}
