// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "The_Arena/UObjects/DefaultDelegates.h"

#include "CharacteristicEffectBase.generated.h"


UENUM(BlueprintType)
enum class ECharacteristicEffectType : uint8
{
	NONE             UMETA(DisplayName = "NONE"),
	Endless          UMETA(DisplayName = "Endless"),
	Infinity         UMETA(DisplayName = "Infinity"),
};

USTRUCT(BlueprintType)
struct FCharacteristicEffectParams
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName EffectName = "NONE";
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ECharacteristicEffectType EffectType = ECharacteristicEffectType::NONE;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ActionTime = -1;
};

UCLASS()
class THE_ARENA_API UCharacteristicEffectBase : public UActorComponent
{
	GENERATED_BODY()

	//Methods
public:
	void CharacteristicEffectBaseInitialization(FCharacteristicEffectParams InEffectParams);

	FDMD& GetRateDispatcher();
	
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FName GetEffectName() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	ECharacteristicEffectType GetEffectType() const;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetActionTime() const;

	UFUNCTION(BlueprintCallable)
	void CreateEffect();
	UFUNCTION(BlueprintCallable)
	void RemoveEffect();
	
protected:
	virtual void BeginPlay() override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	virtual void CreateEffectInternal();
	virtual void OnRate();
	virtual void RemoveEffectInternal();

	void CreateEffectInfoWidget();
	void RemoveEffectInfoWidget();

	UFUNCTION()
	void OnRep_EffectCreated();
	//Fields
protected:
	UPROPERTY(Replicated)
	FCharacteristicEffectParams EffectParams;
	UPROPERTY(ReplicatedUsing = OnRep_EffectCreated)
	bool bEffectCreated = false;

	UPROPERTY()
	float Rate = 0.1f;

	FTimerHandle RemoveEffectTimerHandle;
	FTimerHandle RateTimeHandle;

	//Events
	UPROPERTY(BlueprintAssignable)
	FDMD RateDispatcher;
};
