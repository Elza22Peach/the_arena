// Fill out your copyright notice in the Description page of Project Settings.


#include "StatEffectBaseComponent.h"

void UStatEffectBaseComponent::StatEffectBaseInitialization(TSubclassOf<UStatBaseComponent> InStatComponentClass,
	FCharacteristicEffectParams InEffectParams)
{
	StatComponentClass = InStatComponentClass;
	
	if (auto Owner = GetOwner())
	{
		auto LocalStatComponent = Cast<UStatBaseComponent>(Owner->GetComponentByClass(InStatComponentClass));

		if (LocalStatComponent)
		{
			StatComponent = LocalStatComponent;
		}
	}

	CharacteristicEffectBaseInitialization(InEffectParams);
}

UStatBaseComponent* UStatEffectBaseComponent::GetStatComponent() const
{
	return StatComponent;
}
