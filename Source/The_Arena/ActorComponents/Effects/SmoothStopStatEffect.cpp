// Fill out your copyright notice in the Description page of Project Settings.


#include "SmoothStopStatEffect.h"

#include "The_Arena/ActorComponents/HealthComponent.h"
#include "The_Arena/ActorComponents/Effects/AccuringEffectComponent.h"


void USmoothStopStatEffect::SmoothStopStatEffectInitialization(
	TSubclassOf<UStatEffectBaseComponent> InStatEffectComponentClass, float InAmountEffectPerSec,
	FCharacteristicEffectParams InEffectParams)
{
	StatEffectComponentClass = InStatEffectComponentClass;
	AmountEffectPerSec = InAmountEffectPerSec;

	CharacteristicEffectBaseInitialization(InEffectParams);
}

void USmoothStopStatEffect::OnRate()
{
	Super::OnRate();
	
	SmoothRemoveStatEffect();
}

void USmoothStopStatEffect::SmoothRemoveStatEffect()
{
	if (auto Owner = GetOwner())
	{
		if (auto StatEffectComponent = Cast<UStatEffectBaseComponent>(Owner->GetComponentByClass(StatEffectComponentClass)))
		{
			if(auto StatComponent = StatEffectComponent->GetStatComponent())
			{
				if (auto Effect = StatComponent->FindBonusEffect(StatEffectComponent->GetEffectName()))
				{
					Effect->Value = Effect->Value + AmountEffectPerSec * Rate;

					if (FMath::IsNearlyZero(Effect->Value, 0.01f) || Effect->Value >= 0)
					{
						StatEffectComponent->RemoveEffect();
					}
				}
			}
		}
	}
}