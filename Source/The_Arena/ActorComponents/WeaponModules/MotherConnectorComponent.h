// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/ActorComponents/WeaponModules/BaseConnectorComponent.h"
#include "The_Arena/UObjects/DefaultDelegates.h"

#include "MotherConnectorComponent.generated.h"


class UFatherConnectorComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class THE_ARENA_API UMotherConnectorComponent : public UBaseConnectorComponent
{
	GENERATED_BODY()

	//Methods
public:
	FDMD& GetSetupAttachmentDispatcher();
	FDMD& GetRemoveAttachmentDispatcher();
	
	TArray<TSubclassOf<UFatherConnectorComponent>> GetAcceptedFatherConnectorClasses() const;
	
	void SetupAttachment(UFatherConnectorComponent* FatherConnector);
	void RemoveAttachment();

	AActor* GetAttachedModule() const;
	UFatherConnectorComponent* GetAttachedFatherConnector() const;
protected:
	UMotherConnectorComponent();
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly)
	TArray<TSubclassOf<UFatherConnectorComponent>> AcceptedFatherConnectorClasses;

	UPROPERTY(Replicated)
	UFatherConnectorComponent* AttachedFatherConnector;
	UPROPERTY(Replicated)
	AActor* AttachedModule;

	//Events
	UPROPERTY(BlueprintAssignable)
	FDMD SetupAttachmentDispatcher;
	UPROPERTY(BlueprintAssignable)
	FDMD RemoveAttachmentDispatcher;
};
