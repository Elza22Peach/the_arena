// Fill out your copyright notice in the Description page of Project Settings.


#include "ModuleItemObjectParams.h"
#include "The_Arena/ActorComponents/WeaponModules/ModuleItemObjectsManager.h"
#include "The_Arena/Actors/InventoryItemObject.h"

FIntPoint UModuleItemObjectParams::GetAdditionalDimensions() const
{
	FIntPoint AdditionalDimensionsOfModules = {0,0};
	
	if(auto ModuleItemObjectsManager = GetOwner()->FindComponentByClass<UModuleItemObjectsManager>())
	{
		auto ModuleItemObjects = ModuleItemObjectsManager->GetModuleItemObjects();

		for(auto ModuleItemObject : ModuleItemObjects)
		{
			if(ModuleItemObject)
			{
				if(auto ModuleItemObjectParams = ModuleItemObject->FindComponentByClass<UModuleItemObjectParams>())
				{
					AdditionalDimensionsOfModules += ModuleItemObjectParams->GetAdditionalDimensions();
				}
			}
		}
	}
	
	return AdditionalDimensions + AdditionalDimensionsOfModules;
}

FVector UModuleItemObjectParams::GetAdditionalSceneCaptureLocation() const
{
	FVector AdditionalSceneCaptureLocationOfModules = FVector::ZeroVector;

	if (auto ModuleItemObjectsManager = GetOwner()->FindComponentByClass<UModuleItemObjectsManager>())
	{
		auto ModuleItemObjects = ModuleItemObjectsManager->GetModuleItemObjects();

		for (auto ModuleItemObject : ModuleItemObjects)
		{
			if (ModuleItemObject)
			{
				if (auto ModuleItemObjectParams = ModuleItemObject->FindComponentByClass<UModuleItemObjectParams>())
				{
					AdditionalSceneCaptureLocationOfModules += ModuleItemObjectParams->GetAdditionalSceneCaptureLocation();
				}
			}
		}
	}

	return AdditionalSceneCaptureLocation + AdditionalSceneCaptureLocationOfModules;
}

// Sets default values for this component's properties
UModuleItemObjectParams::UModuleItemObjectParams()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UModuleItemObjectParams::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UModuleItemObjectParams::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

