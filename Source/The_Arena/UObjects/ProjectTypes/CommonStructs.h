// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "UObject/NoExportTypes.h"

#include "CommonStructs.generated.h"


//////////////////////////////////////////////////////////
/** Utility class for struct types */
UCLASS()
class THE_ARENA_API UCommonStructs : public UObject
{
	GENERATED_BODY()
	
};
//////////////////////////////////////////////////////////