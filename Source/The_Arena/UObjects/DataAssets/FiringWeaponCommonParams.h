// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "The_Arena/UObjects/DataAssets/InventoryItemCommonParams.h"

#include "FiringWeaponCommonParams.generated.h"


UENUM(BlueprintType)
enum EFiringWeaponType {Pistol, Shotgun, SubmachineGun, Rifle, SniperRifle};

UCLASS(BlueprintType)
class THE_ARENA_API UFiringWeaponCommonParams : public UInventoryItemCommonParams
{
	GENERATED_BODY()

	//Methods
public:
	UFUNCTION(BlueprintCallable, BlueprintPure)
	TEnumAsByte<EFiringWeaponType> GetFiringWeaponType() const;
	
	//Fields
protected:
	UPROPERTY(EditDefaultsOnly, Category = "ItemCommonParametrs")
	TEnumAsByte<EFiringWeaponType> FiringWeaponType;
};
