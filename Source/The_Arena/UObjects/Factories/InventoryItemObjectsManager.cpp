// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryItemObjectsManager.h"


AInventoryItemObject* UInventoryItemObjectsManager::CreateInventoryItemObject(FName ItemObjectName,
	const FActorSpawnParameters& SpawnParameters, UInventoryItemParam* ItemParam)
{
	if (!InventoryItemObjectAsset)
		return nullptr;

	auto ItemObjectClass = InventoryItemObjectAsset->GetInventoryItemObjectClass(ItemObjectName);

	return CreateInventoryItemObject(ItemObjectClass, SpawnParameters, ItemParam);
}

AInventoryItemObject* UInventoryItemObjectsManager::CreateInventoryItemObject(UClass* ItemObjectClass,
	const FActorSpawnParameters& SpawnParameters, UInventoryItemParam* ItemParam)
{
	if (!ItemObjectClass)
		return nullptr;

	auto ItemObject = GetWorld()->SpawnActor<AInventoryItemObject>(ItemObjectClass, SpawnParameters);

	if (ItemObject)
	{
		//ItemObject->bAlwaysRelevant = true;
		
		if (SpawnParameters.Owner)
		{
			ItemObject->AttachToActor(SpawnParameters.Owner, FAttachmentTransformRules::KeepRelativeTransform);
		}

		if (ItemParam)
		{
			ItemObject->Initialization(ItemParam);
		}
	}

	return ItemObject;
}

UInventoryItemObjectAssetType* UInventoryItemObjectsManager::GetInventoryItemObjectAsset() const
{
	return InventoryItemObjectAsset;
}
