// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "The_Arena/Actors/DroppedItem.h"

#include "DroppedItemsManager.generated.h"


UCLASS(Blueprintable)
class THE_ARENA_API UDroppedItemsManager : public UObject
{
	GENERATED_BODY()

	//Methods
public:
	UFUNCTION(BlueprintCallable)
		void SpawnInventoryItemInFrontOfTargetActor(AInventoryItemObject* InventoryItemObject, AActor* TargetActor, bool GroundClamp = false, float SpawnDistanceFromActor = 150.0f);
	
	ADroppedItem* CreateDroppedItem(UClass* Class, const FActorSpawnParameters& SpawnParameters = FActorSpawnParameters(), UInventoryItemParam* InventoryItemParam = nullptr);
	ADroppedItem* CreateDroppedItem(UClass* Class, FVector const& Location, FRotator const& Rotation, const FActorSpawnParameters& SpawnParameters = FActorSpawnParameters(), UInventoryItemParam* InventoryItemParam = nullptr);
};
