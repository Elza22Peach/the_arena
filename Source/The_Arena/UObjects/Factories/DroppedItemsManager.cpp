// Fill out your copyright notice in the Description page of Project Settings.


#include "DroppedItemsManager.h"

#include "The_Arena/Actors/InventoryItemObject.h"
#include "The_Arena/UObjects/DataAssets/InventoryItemCommonParams.h"
#include "The_Arena/ActorComponents/WeaponModules/ModuleItemObjectsManager.h"


void UDroppedItemsManager::SpawnInventoryItemInFrontOfTargetActor(AInventoryItemObject* InventoryItemObject,
	AActor* TargetActor, bool GroundClamp, float SpawnDistanceFromActor)
{
	if (!InventoryItemObject || !TargetActor)
		return;

	FVector ItemLocation = TargetActor->GetActorLocation() + TargetActor->GetActorForwardVector() * SpawnDistanceFromActor;
	FRotator ItemRotation = FRotator(FMath::RandRange(0, 360), FMath::RandRange(0, 360), FMath::RandRange(0, 360));
	if (GroundClamp)
	{
		FHitResult OutHit;
		if (GetWorld()->LineTraceSingleByChannel(OutHit, ItemLocation, ItemLocation - FVector(0.0f, 0.0f, 2000.0f), ECollisionChannel::ECC_Visibility))
		{
			ItemLocation = OutHit.Location;
		}
	}

	if (auto ItemCommonParams = InventoryItemObject->GetInventoryItemCommonParams())
	{
		auto InventoryItemParam = InventoryItemObject->GetInventoryItemParam();
		
		if(auto ModuleItemObjectsManager = InventoryItemObject->FindComponentByClass<UModuleItemObjectsManager>())
		{
			ModuleItemObjectsManager->ClearModuleItemObjectsArray();
		}
		
		FActorSpawnParameters SpawnParameters;
		CreateDroppedItem(ItemCommonParams->GetClassOfDroppedItem(), ItemLocation, ItemRotation, SpawnParameters, InventoryItemParam);
	}
}

ADroppedItem* UDroppedItemsManager::CreateDroppedItem(UClass* Class, const FActorSpawnParameters& SpawnParameters, UInventoryItemParam* InventoryItemParam)
{
	return CreateDroppedItem(Class, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParameters, InventoryItemParam);
}

ADroppedItem* UDroppedItemsManager::CreateDroppedItem(UClass* Class, FVector const& Location, FRotator const& Rotation,
	const FActorSpawnParameters& SpawnParameters, UInventoryItemParam* InventoryItemParam)
{
	if (!Class)
		return nullptr;

	auto DroppedItem = GetWorld()->SpawnActor<ADroppedItem>(Class, Location, Rotation, SpawnParameters);

	if (DroppedItem)
	{
		if (InventoryItemParam)
		{
			DroppedItem->Initialization(InventoryItemParam);
		}
	}

	return DroppedItem;
}
