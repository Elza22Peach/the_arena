// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryItemsManager.h"

#include "The_Arena/ActorComponents/WeaponModules/ModuleItemObjectsManager.h"
#include "The_Arena/UObjects/DataAssets/InventoryItemCommonParams.h"

AInventoryItem* UInventoryItemsManager::CreateInventoryItem(UClass* Class,
	const FActorSpawnParameters& SpawnParameters, const FInventoryItemParams& ItemParams)
{
	if (!Class)
		return nullptr;

	auto InventoryItem = GetWorld()->SpawnActor<AInventoryItem>(Class, SpawnParameters);

	if (InventoryItem)
	{
		InventoryItem->MulticastInitialization(ItemParams.bTestItem);
		
		if(SpawnParameters.Owner)
		{
			InventoryItem->AttachToActor(SpawnParameters.Owner, FAttachmentTransformRules::KeepRelativeTransform);
		}
		
		if(auto InventoryItemObject = ItemParams.ItemObject)
		{
			if(auto ModuleItemObjectsManager = InventoryItemObject->FindComponentByClass<UModuleItemObjectsManager>())
			{
				ModuleItemObjectsManager->CreateModuleItems(InventoryItem, ItemParams.bTestItem);
			}
		}
	}

	return InventoryItem;
}
