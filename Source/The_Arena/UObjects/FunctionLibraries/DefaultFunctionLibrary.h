// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "DefaultFunctionLibrary.generated.h"

class AInventoryItemObject;
class UInventoryBaseComponent;

UENUM(BlueprintType)
enum class EBuildConfigurationBP : uint8
{
	/** Unknown build configuration. */
	Unknown,

	/** Debug build. */
	Debug,

	/** DebugGame build. */
	DebugGame,

	/** Development build. */
	Development,

	/** Shipping build. */
	Shipping,

	/** Test build. */
	Test
};

UCLASS()
class THE_ARENA_API UDefaultFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category= "DefaultFunctionLibrary")
		static EBuildConfigurationBP GetConfigurationBuild();
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "DefaultFunctionLibrary")
		static FString GetProjectVersion();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "InventoryItemObject")
		static bool CanItemObjectUsed(AActor* UsedActor, AInventoryItemObject* ItemObject);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "InventoryItemObject")
		static bool CanModuleAttached(AActor* UsedActor, AInventoryItemObject* ModulableItemObject, UInventoryBaseComponent* ModulableItemObjectInventory,  AInventoryItemObject* ModuleItemObject, UInventoryBaseComponent* ModuleItemObjectInventory);
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "InventoryItemObject")
		static bool CanModuleDetached(AActor* UsedActor, AInventoryItemObject* ModulableItemObject);
	UFUNCTION(BlueprintCallable, Category = "InventoryItemObject")
		static void DecrementItemAmount(AActor* UsedActor, AInventoryItemObject* ItemObject);

private:
	static bool CanItemObjectUsedDefault(AActor* UsedActor, AInventoryItemObject* ItemObject);
};
