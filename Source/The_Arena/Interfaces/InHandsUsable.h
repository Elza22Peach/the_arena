// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "The_Arena/Interfaces/ItemUsable.h"
#include "The_Arena/UObjects/DefaultDelegates.h"
#include "The_Arena/Actors/TPInventoryItem.h"

#include "InHandsUsable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInHandsUsable : public UItemUsable
{
	GENERATED_BODY()
};

/**
 * 
 */
class THE_ARENA_API IInHandsUsable : public IItemUsable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void OnFireButtonPressed();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void OnFireButtonReleased();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void OnReloadButtonPressed();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void OnChangeFireModeButtonPressed();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void OnAimingButtonPressed();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void OnAimingButtonReleased();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void OnInclineButton(float Value);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void OnHorizontalMovingButton(float Value);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void Remove();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void BindToRemovedDispatcher(UObject* InObject, const FName& InFunctionName );
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void UnbindFromRemovedDispatcher(UObject* InObject, const FName& InFunctionName);
};
