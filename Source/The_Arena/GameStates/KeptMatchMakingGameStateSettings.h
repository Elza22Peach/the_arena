// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "The_Arena/UObjects/ProjectTypes/CommonEnums.h"

#include "KeptMatchMakingGameStateSettings.generated.h"

class AThe_Arena_MatchMakingGameState;

UCLASS()
class THE_ARENA_API AKeptMatchMakingGameStateSettings : public AActor
{
	GENERATED_BODY()

	//Methods
protected:
	// Sets default values for this actor's properties
	AKeptMatchMakingGameStateSettings();
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	void CopyParametersFrom(AThe_Arena_MatchMakingGameState* InGameState);
	void CopyParametersTo(AThe_Arena_MatchMakingGameState* InGameState);

	//Fields
protected:
	int WonRounds_Team_1;
	int WonRounds_Team_2;
	int EndMatchWithoutPlayersTimePassed;

	EGameTeam LastWonRoundTeam = EGameTeam::NONE;
};
